#!/bin/bash

command=$1
project=$2
name=$3
hostnames=$4
region="${5:-us-east1-b}"

echo ":: IP address and DNS"
ip=$(./ip.sh ${command} ${project} ${name} ${hostnames} root ${region%-*})

echo ":: Nginx Compute instance"
./gce-nginx.sh ${command} ${name} ${project} ${region} ${ip}

#declare -a names=(hgnet-home-ip hgnet-tea-ip hgnet-life-ip)
#declare -a addresses=(hokigeek.net tea.hokigeek.net life.hokigeek.net)

