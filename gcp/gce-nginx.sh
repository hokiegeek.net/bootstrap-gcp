#!/bin/sh

command=$1
name=$2
project=$3
zone=$4
ip=$5

case ${command} in
    create)
        gcloud beta compute --project=${project} instances create-with-container \
            ${name} --zone=${zone} \
            --machine-type=f1-micro \
            --subnet=default --address=${ip} --network-tier=PREMIUM \
            --metadata=google-logging-enabled=true --maintenance-policy=MIGRATE \
            --service-account=232416189918-compute@developer.gserviceaccount.com \
            --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
            --tags=http-server,https-server \
            --image=cos-stable-68-10718-102-0 --image-project=cos-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=${name} \
            --container-image=nginx:alpine --container-restart-policy=always --container-mount-host-path=mount-path=/etc/nginx/conf.d,host-path=/etc/nginx/conf.d,mode=rw \
            --labels=container-vm=cos-stable-68-10718-102-0
        ;;
    remove)
        gcloud beta compute --project=${project} --quiet instances delete \
            ${name} --zone=${zone}
        ;;
esac
