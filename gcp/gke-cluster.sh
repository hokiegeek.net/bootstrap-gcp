#!/bin/sh

name=$1
project=$2
zone="${3:-us-east1-b}"

gcloud container --project "${project}" clusters create "${name}" \
    --zone "${zone}" --username "admin" --cluster-version "1.9.7-gke.6" \
    --machine-type "f1-micro" --image-type "COS" \
    --disk-type "pd-standard" --disk-size "25" \
    --scopes "https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
    --network "projects/${project}/global/networks/default" \
    --subnetwork "projects/${project}/regions/us-east1/subnetworks/default" \
    --enable-cloud-logging --enable-cloud-monitoring \
    --num-nodes "3" --enable-autoscaling --min-nodes "0" --max-nodes "3" \
    --addons HorizontalPodAutoscaling,HttpLoadBalancing,KubernetesDashboard --no-enable-autoupgrade --enable-autorepair

gcloud container clusters get-credentials ${name} --zone ${zone} --project ${project}
