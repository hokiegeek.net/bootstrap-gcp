#!/bin/bash

command=$1
project=$2
name=$3
hostnames=$4
zone=$5
region=$6

addy_name=${project}-${name}-ip

output="--verbosity=none --no-user-output-enabled"

dns() {
    local command=$1
    local project=$2
    local ip=$3
    local hostnames=$4
    local zone=$5

    oldIFS=$IFS
    IFS=',' read -r -a hostnames_arr <<< $hostnames
    IFS=$oldIFS

    ret=0

    gcloud dns --project=${project} ${output} record-sets transaction start --zone=${zone} || ret=1
    for i in "${!hostnames_arr[@]}"; do
        hostname="${hostnames_arr[i]}"
        if (( i == 0 )); then
            gcloud dns --project=${project} ${output} record-sets transaction ${command} ${ip} --name="${hostname}." --ttl=300 --type=A --zone=${zone} || ret=1
        else
            gcloud dns --project=${project} ${output} record-sets transaction ${command} ${hostnames_arr[0]}. --name="${hostname}." --ttl=300 --type=CNAME --zone=${zone} || ret=1
        fi
    done
    gcloud dns --project=${project} ${output} record-sets transaction execute --zone=${zone} || ret=1

    (( ret != 0 )) && gcloud dns --project=${project} record-sets transaction abort --zone=${zone}

    return $ret
}

case ${command} in
    create)
        gcloud compute addresses create ${output} ${addy_name} --region=${region}

        ip=$(gcloud compute addresses describe ${addy_name} --region=${region} --format="value(address)")

        dns add ${project} ${ip} ${hostnames} ${zone} || {
            gcloud compute addresses delete ${addy_name} --quiet --region=${region}
            exit 1
        }

        echo ${ip}
        ;;
    remove)
        ip=$(gcloud compute addresses describe ${addy_name} --region=${region} --format="value(address)")

        dns remove ${project} ${ip} ${hostnames} ${zone} \
            && gcloud compute addresses delete ${addy_name} --quiet --region=${region}
        ;;
esac
