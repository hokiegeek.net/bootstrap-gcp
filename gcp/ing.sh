#!/bin/sh

[[ $1 != "delete" ]] && {
    kubectl create clusterrolebinding cluster-admin-binding \
          --clusterrole cluster-admin \
          --user $(gcloud config get-value account)
}

kubectl ${1:-apply} -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml || exit 1
kubectl ${1:-apply} -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml || exit 2
echo "Kill this when things start showing up..."
kubectl get pods --all-namespaces --watch
