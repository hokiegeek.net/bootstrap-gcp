#!/bin/sh

gcloud dns --project=hgnet-tea record-sets transaction start --zone=root

gcloud dns --project=hgnet-tea record-sets transaction add --name=hokiegeek.net. --ttl=300 --type=A --zone=root

gcloud dns --project=hgnet-tea record-sets transaction execute --zone=root
