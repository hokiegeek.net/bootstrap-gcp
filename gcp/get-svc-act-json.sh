#!/bin/sh

accountid=$1
project=$2
gcloud iam service-accounts describe ${accountid}@${project}.iam.gserviceaccount.com --format=json
# gcloud iam service-accounts keys list --iam-account ${accountid}@${project}.iam.gserviceaccount.com
