package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"net/http"
	// "net/http/httputil"
	"time"
)

type gitlabProject struct {
	ID                int           `json:"id"`
	Description       string        `json:"description"`
	Name              string        `json:"name"`
	NameWithNamespace string        `json:"name_with_namespace"`
	Path              string        `json:"path"`
	PathWithNamespace string        `json:"path_with_namespace"`
	CreatedAt         time.Time     `json:"created_at"`
	DefaultBranch     string        `json:"default_branch"`
	TagList           []interface{} `json:"tag_list"`
	SSHURLToRepo      string        `json:"ssh_url_to_repo"`
	HTTPURLToRepo     string        `json:"http_url_to_repo"`
	WebURL            string        `json:"web_url"`
	ReadmeURL         string        `json:"readme_url"`
	AvatarURL         interface{}   `json:"avatar_url"`
	StarCount         int           `json:"star_count"`
	ForksCount        int           `json:"forks_count"`
	LastActivityAt    time.Time     `json:"last_activity_at"`
	Namespace         struct {
		ID       int         `json:"id"`
		Name     string      `json:"name"`
		Path     string      `json:"path"`
		Kind     string      `json:"kind"`
		FullPath string      `json:"full_path"`
		ParentID interface{} `json:"parent_id"`
	} `json:"namespace"`
	Links struct {
		Self          string `json:"self"`
		Issues        string `json:"issues"`
		MergeRequests string `json:"merge_requests"`
		RepoBranches  string `json:"repo_branches"`
		Labels        string `json:"labels"`
		Events        string `json:"events"`
		Members       string `json:"members"`
	} `json:"_links"`
	Archived                                  bool          `json:"archived"`
	Visibility                                string        `json:"visibility"`
	ResolveOutdatedDiffDiscussions            bool          `json:"resolve_outdated_diff_discussions"`
	ContainerRegistryEnabled                  bool          `json:"container_registry_enabled"`
	IssuesEnabled                             bool          `json:"issues_enabled"`
	MergeRequestsEnabled                      bool          `json:"merge_requests_enabled"`
	WikiEnabled                               bool          `json:"wiki_enabled"`
	JobsEnabled                               bool          `json:"jobs_enabled"`
	SnippetsEnabled                           bool          `json:"snippets_enabled"`
	SharedRunnersEnabled                      bool          `json:"shared_runners_enabled"`
	LfsEnabled                                bool          `json:"lfs_enabled"`
	CreatorID                                 int           `json:"creator_id"`
	ImportStatus                              string        `json:"import_status"`
	OpenIssuesCount                           int           `json:"open_issues_count"`
	PublicJobs                                bool          `json:"public_jobs"`
	CiConfigPath                              interface{}   `json:"ci_config_path"`
	SharedWithGroups                          []interface{} `json:"shared_with_groups"`
	OnlyAllowMergeIfPipelineSucceeds          bool          `json:"only_allow_merge_if_pipeline_succeeds"`
	RequestAccessEnabled                      bool          `json:"request_access_enabled"`
	OnlyAllowMergeIfAllDiscussionsAreResolved bool          `json:"only_allow_merge_if_all_discussions_are_resolved"`
	PrintingMergeRequestLinkEnabled           bool          `json:"printing_merge_request_link_enabled"`
	MergeMethod                               string        `json:"merge_method"`
	ApprovalsBeforeMerge                      int           `json:"approvals_before_merge"`
	Mirror                                    bool          `json:"mirror"`
}

type gitlabTag struct {
	Commit struct {
		ID             string    `json:"id"`
		ShortID        string    `json:"short_id"`
		Title          string    `json:"title"`
		CreatedAt      time.Time `json:"created_at"`
		ParentIds      []string  `json:"parent_ids"`
		Message        string    `json:"message"`
		AuthorName     string    `json:"author_name"`
		AuthorEmail    string    `json:"author_email"`
		AuthoredDate   string    `json:"authored_date"`
		CommitterName  string    `json:"committer_name"`
		CommitterEmail string    `json:"committer_email"`
		CommittedDate  string    `json:"committed_date"`
	} `json:"commit"`
	Release struct {
		TagName     string `json:"tag_name"`
		Description string `json:"description"`
	} `json:"release"`
	Name    string      `json:"name"`
	Target  string      `json:"target"`
	Message interface{} `json:"message"`
}

type gitlabPipeline struct {
	ID         int         `json:"id"`
	Sha        string      `json:"sha"`
	Ref        string      `json:"ref"`
	Status     string      `json:"status"`
	BeforeSha  string      `json:"before_sha"`
	Tag        bool        `json:"tag"`
	YamlErrors interface{} `json:"yaml_errors"`
	User       struct {
		Name      string `json:"name"`
		Username  string `json:"username"`
		ID        int    `json:"id"`
		State     string `json:"state"`
		AvatarURL string `json:"avatar_url"`
		WebURL    string `json:"web_url"`
	} `json:"user"`
	CreatedAt   time.Time   `json:"created_at"`
	UpdatedAt   time.Time   `json:"updated_at"`
	StartedAt   interface{} `json:"started_at"`
	FinishedAt  interface{} `json:"finished_at"`
	CommittedAt interface{} `json:"committed_at"`
	Duration    interface{} `json:"duration"`
	Coverage    interface{} `json:"coverage"`
	WebURL      string      `json:"web_url"`
}

/*
type gitlabReq struct {
	url  string
	resp chan gitlabResp
}

type gitlabResp struct {
	json json.Decoder
	err  error
}
*/

type triggeredBuild struct {
	project    gitlabProject
	tag        string
	pipelineID int
	status     string
}

func main() {
	gitlabTokenPtr := flag.String("token", "", "The GitLab token")
	groupPtr := flag.String("group", "", "The GitLab group")

	flag.Parse()

	/*
		requests := make(chan gitlabReq)
		defer close(requests)

		go func() {
			client := &http.Client{}

			for glreq := range requests {
				fullURL := fmt.Sprintf("https://gitlab.com/api/v4/%s", glreq.url)

				req, err := http.NewRequest("GET", fullURL, nil)
				if err != nil {
					return
				}
				req.Header.Set("PRIVATE-TOKEN", *gitlabTokenPtr)
				res, err := client.Do(req)
				if err != nil {
					return
				}

				glreq.resp <- gitlabResp{json: *json.NewDecoder(res.Body), err: err}
			}
		}()
	*/

	fmt.Println("Retrieving projects....")
	projects, err := getProjects(*gitlabTokenPtr, *groupPtr)
	if err != nil {
		panic(err.Error())
	}

	completed := make(chan bool)
	triggered := make(chan *triggeredBuild)

	go func() {
		var builds []*triggeredBuild

		ticker := time.NewTicker(1 * time.Second)
		for _ = range ticker.C {
			fmt.Print("\033[2J")
			fmt.Print("\033[1;1H")

			if t, more := <-triggered; more {
				builds = append(builds, t)
			}

			done := true
			for _, b := range builds {
				fmt.Printf("%3d %-17s %-10s %s\n", b.project.ID, b.project.Name, b.tag, b.status)
				if b.status == "running" || b.status == "pending" {
					done = false
				}
			}

			if done {
				completed <- true
				return
			}
		}
	}()

	for _, p := range projects {
		// fmt.Printf("%s: found\n", p.Name)
		if tag, err := getLatestProjectTag(*gitlabTokenPtr, p.ID); err != nil {
			fmt.Println(err.Error())
		} else {
			triggerBuild(*gitlabTokenPtr, p, tag, triggered)
		}
	}
	close(triggered)

	<-completed
}

func gitlabReq(gitlabToken, url string) (*json.Decoder, error) {
	fullURL := fmt.Sprintf("https://gitlab.com/api/v4/%s", url)

	req, err := http.NewRequest("GET", fullURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("PRIVATE-TOKEN", gitlabToken)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return json.NewDecoder(res.Body), nil
}

func getProjects(gitlabToken, group string) (projects []gitlabProject, err error) {
	// req := gitlabReq{url: fmt.Sprintf("groups/%s/projects", group), resp: make(chan gitlabResp)}
	// requests <- req
	// resp := <-req.resp

	json, err := gitlabReq(gitlabToken, fmt.Sprintf("groups/%s/projects", group))
	if err != nil {
		return
	}

	json.Decode(&projects)
	return
}

func getLatestProjectTag(gitlabToken string, projectID int) (string, error) {
	// req := gitlabReq{url: fmt.Sprintf("projects/%d/repository/tags", projectID), resp: make(chan gitlabResp)}
	// requests <- req
	// resp := <-req.resp
	json, err := gitlabReq(gitlabToken, fmt.Sprintf("projects/%d/repository/tags", projectID))
	if err != nil {
		return "", err
	}

	var tags []gitlabTag
	if err := json.Decode(&tags); err != nil {
		return "", err
	}

	if len(tags) == 0 {
		return "", errors.New("No tags found")
	}

	return tags[0].Name, nil
}

func pipelineStatus(gitlabToken string, b *triggeredBuild) {
	/*
		time.Sleep(12 * time.Second)
		b.status = "success"
	*/

	ticker := time.NewTicker(10 * time.Second)
	for _ = range ticker.C {
		if b.pipelineID > -1 {
			url := fmt.Sprintf("projects/%d/pipeline/%d", b.project.ID, b.pipelineID)
			json, err := gitlabReq(gitlabToken, url)
			if err != nil {
				return
			}

			var pipeline gitlabPipeline
			json.Decode(&pipeline)

			if len(pipeline.Status) == 0 {
				fmt.Println(url)
				fmt.Printf("%v\n", pipeline)
				panic(42)
			}

			if b.status != "running" && b.status != "pending" {
				ticker.Stop()
			}

			b.status = pipeline.Status
		}
	}

}

func triggerBuild(gitlabToken string, project gitlabProject, tag string, triggered chan<- *triggeredBuild) {
	b := &triggeredBuild{project: project, tag: tag, pipelineID: -1, status: "pending"}
	triggered <- b

	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%d/pipeline?ref=%s", project.ID, tag)
	// fmt.Println(fmt.Sprintf("https://gitlab.com/api/v4/projects/%d/pipeline?ref=%s", project.ID, tag))

	client := http.Client{}
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		b.status = fmt.Sprintf("ERROR: %s", err)
		return
	}
	req.Header.Set("PRIVATE-TOKEN", gitlabToken)

	// output, err := httputil.DumpRequest(req, true)
	// if err != nil {
	// 	fmt.Println("Error dumping request:", err)
	// 	return
	// }
	// fmt.Println(string(output))

	resp, err := client.Do(req)
	if err != nil {
		b.status = fmt.Sprintf("ERROR: %s", err)
		return
	}

	// if resp.StatusCode != http.StatusCreated {
	// 	fmt.Println(resp.Status)
	// 	panic(42)
	// }

	var pipeline gitlabPipeline
	json.NewDecoder(resp.Body).Decode(&pipeline)

	if len(pipeline.Status) == 0 {
		fmt.Printf("%v\n", pipeline)
		panic(42)
	}

	b.pipelineID = pipeline.ID
	b.status = pipeline.Status
	go pipelineStatus(gitlabToken, b)
}
