#!/bin/sh

token=$1
group=$2
    # awk '/name_with_namespace/ { gsub(/"/, ""); sub(/ \/ /, "%20", $2); print $2 }' RS=, FS=: | \
curl --request GET --header "PRIVATE-TOKEN: ${token}" "https://gitlab.com/api/v4/groups/${group}/projects"

# curl --request GET --header "PRIVATE-TOKEN: ${token}" "https://gitlab.com/api/v4/groups/${group}/projects" | \
#     sed 's/namespace:{[^}]*}//g' | awk '$1 ~ /"id"/ { print $2 }' RS=, FS=: | \
#     while read project; do
#         [[ ${project} == "5467267" ]] && continue
# 
#         refs=()
#         case ${project} in
#             5458893) refs+=("latest" "release-next" "v5.6.1") ;;
#             5458504) refs+=("develop" "main" "release/0.26.0") ;;
#             5404962) refs+=("master" "stable-2.89" "jenkins-2.106") ;;
#         esac
# 
#         # ref=$(curl --request GET --header "PRIVATE-TOKEN: ${token}" "https://gitlab.com/api/v4/projects/${project}/repository/branches" 2>&1 | awk '$1 ~ /"name"/ { gsub(/"/, "", $2); name=$2 } $1 ~ /"protected"/ { if ($2 == "true") print name } ' RS=, FS=:)
#         # echo "${project}: ${ref:=master}"
#         echo "${project}"
#         for ref in ${refs[@]}; do
#            curl --silent --request POST --header "PRIVATE-TOKEN: ${token}" "https://gitlab.com/api/v4/projects/${project}/pipeline?ref=${ref}"
#         done
#         # curl --request GET --header "PRIVATE-TOKEN: ${token}" "https://gitlab.com/api/v4/projects/${project}/repository/branches?per_page=100" 2>&1 | awk '$1 ~ /"name"/ { gsub(/"/, "", $2); name=$2 } $1 ~ /"protected"/ { if ($2 == "true") print name } ' RS=, FS=:
#     done
